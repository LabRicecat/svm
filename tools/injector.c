#include <stdio.h>
#include <stdlib.h>

void usage(void) {
    fputs("Usage: injector <src> <dst> [pos=0] [num=all]\n", stderr);
    exit(EXIT_FAILURE);
}

size_t stoi(const char* c) {
    size_t num = 0, i = 0;
    while(c[i])
        num = num * 10 + c[i++] - '0';

    return num;
}

int isgood(FILE* f) {
    int c;
    if((c = getc(f)) == EOF)
        return 0;
    
    ungetc(c, f);
    return 1;
}

int main(int argc, char** argv) {
    FILE* src;
    FILE* dst;
    size_t pos = 0;
    size_t num = 0;

    if(argc < 3 || argc > 5)
        usage();
    if(argc >= 3) {
        src = fopen(argv[1], "r");
        dst = fopen(argv[2], "r+");

        if(!src || !dst)
            fputs("Problems opening files\n", stderr), 
            exit(EXIT_FAILURE);
    }
    if(argc >= 4)
        pos = stoi(argv[3]);
    if(argc >= 5)
        num = stoi(argv[4]);


    for(size_t i = 0; num == 0 ? (isgood(src)) : (i < num); ++i)
        fputc(fgetc(src), dst);

    fclose(src);
    fclose(dst);
}
