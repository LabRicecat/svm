CFLAGS=-g 

SVM_SRC=$(wildcard svm-src/*.c svm-src/devices/*.c)
SAM_SRC=$(wildcard sam-src/*.c)

SVM_CFLAGS=-lncurses
SAM_CFLAGS=$(SVM_CFLAGS)

svm: $(SVM_SRC) $(wildcard svm-src/main/*.c)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(SVM_CFLAGS) -o $@ $^

sam: $(SVM_SRC) $(SAM_SRC) $(wildcard sam-src/main/*.c)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(SAM_CFLAGS) -o $@ $^

injector: tools/injector.c 
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $^
