#include <stdio.h>
#include <stdlib.h>

#include "../def.h"
#include "../stream.h"
#include "../success.h"
#include "../line.h"

#define DEFAULT_FOUT "a.out"

void usage(void) {
    printf("Usage: sam <in> [out]\n");
    exit(1);
}

void print_success(success_t* sc) {
    for(size_t i = 0; i < sc->errors; ++i)
        printf("-> %s at (%d:%d)\n", sc->errorv[i].msg, sc->errorv[i].pos.line, sc->errorv[i].pos.column);
}

int main(int argc, char** argv) {
    char* fout = DEFAULT_FOUT;
    char* fin;
    if(argc == 1)
        usage();
    if(argc >= 2)
        fin = argv[1];
    if(argc == 3)
        fout = argv[2];

    output out = oopen(fout);
    stream_t s = sstart(stream_file(fin));
    success_t sc = mksuccess();

    while(isgood(&sc) && !empty())
        line(&sc);

    if(isbad(&sc)) {
        puts("An error occured: \n");
        print_success(&sc);
    }
    
    rmsuccess(&sc);
    oclose(out);
}
