#include "success.h"
#include "position.h"
#include <stdlib.h>

success_t mksuccess(void) {
    success_t s;
    s.errors = 0;
    s.errorv = malloc(sizeof(struct error_t) * ERROR_TRACE_SIZE);
    s.status = GOOD;
    return s;
}

void rmsuccess(success_t* sc) {
    free(sc->errorv);
    sc->errors = 0;
    sc->status = BAD;
}

success_t* good(success_t* s) {
    if(s) {
        s->status = GOOD; 
        s->errors = 0; 
    }
    return s; 
}

int isgood(success_t* s) { 
    if(s) return s->status == GOOD; 
    return 0; 
}
success_t* bad(success_t* s, char* msg) { 
    if(s) { 
        s->status = BAD;
        s->errorv[s->errors].msg = msg;
        s->errorv[s->errors].pos = cpos();
        ++s->errors;
    }
    return s; 
}
int isbad(success_t* s) { 
    if(s) return s->status == BAD; 
    return BAD; 
}


