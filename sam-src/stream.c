#include "stream.h"
#include <limits.h>
#include <stdlib.h>

output oopen(const char* file) {
    static output o = NULL;
    if(file != NULL && o != NULL)
        fclose(o);
    
    if(file != NULL)
        o = fopen(file, "w+");

    return o;
}

void oclose(output o) {
    fclose(o);
}

void emit(sv_byte b) {
    fputc(b, oopen(NULL));
}

void emit32(sv_word b) {
    for(size_t i = 0; i < sizeof(sv_word) / sizeof(sv_byte); ++i) 
        fputc(b >> (sizeof(sv_word) - i - 1)*CHAR_BIT, oopen(NULL));
}

stream_t* get_stream(void) {
    static stream_t s;
    return &s;
}

stream_t sstart(stream_t s) {
    static stream_t start;
    if(!s)
        return start;

    return *get_stream() = start = s;
}

stream_t at(int c) { 
    return *get_stream() + c; 
}

char incr(int c) { 
    return *at(c); }

char peek(void) { 
    return incr(0); 
}

char next(void) {
    return incr(1); 
}

char eat(void) { 
    return ++*get_stream(), incr(-1); 
}

char back(void) { 
    return *--*get_stream(); 
}

void backby(size_t n) { 
    while(n--) --*get_stream(); 
}

size_t left(void) { 
    size_t lt = 0;
    stream_t cpy = *get_stream();
    while(*cpy)
        ++lt, ++cpy;

    return lt;
}

int empty(void) { 
    return left() == 0; 
}

stream_t stream_file(const char* file) {
    size_t size = 0;
    stream_t current, start;
    FILE* srm = fopen(file, "r");
    if(!srm) return NULL;

    while(getc(srm) != EOF)
        ++size;
    rewind(srm);

    start = current = malloc(size);
    int c;
    while((c = getc(srm)) != EOF)
        *current++ = c;

    return start;
}
