#ifndef VALUE_H
#define VALUE_H

#include "def.h"

char* identf(success_t* sc);
char* identf_instr(success_t* sc);

sv_word decimal(success_t* sc);

int ishex(int x);

sv_word hex(success_t* sc);

int binary(success_t* sc);

sv_word number(success_t* sc); 

sv_fd regist(success_t* sc);

sv_fd devicedesc(success_t* sc);

sv_fd address(success_t* sc);


#endif
