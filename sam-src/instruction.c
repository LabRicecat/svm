#include "instruction.h"
#include "stream.h"
#include "success.h"
#include "blank.h"
#include "value.h"
#include "../svm-src/devices/CPU.h"

#include <stdlib.h>
#include <string.h>

void instr_nop(success_t* sc) {
    emit(sv_cpu_iNOP);
}

void instr_int(success_t* sc) {
    emit(sv_cpu_iINTER);
}

void instr_push(success_t* sc) {
    sv_word val = 0;
    whitespace(sc);
    if(isbad(sc))
        goto error;

    val = address(sc);
    if(isbad(sc))
        goto error;

    emit(sv_cpu_iPUSH);
    emit32(val);
    return;

error:
    bad(sc, "not a push instruction");
}

void instr_pop(success_t* sc) {
    emit(sv_cpu_iPOP);
}

void instr_add(success_t* sc) {
    emit(sv_cpu_iADD);
}

void instr_sub(success_t* sc) {
    emit(sv_cpu_iSUB);
}

void instr_mul(success_t* sc) {
    emit(sv_cpu_iMUL);
}

void instr_div(success_t* sc) {
    emit(sv_cpu_iDIV);
}

void instr_put(success_t* sc) {
    emit(sv_cpu_iPUT);
}

void instr_put8(success_t* sc) {
    emit(sv_cpu_iPUT8);
}

void instr_put32(success_t* sc) {
    emit(sv_cpu_iPUT32);
}

void instr_deref(success_t* sc) {
    emit(sv_cpu_iDEREF);
}

void instr_jmp(success_t* sc) {
    sv_word dst = 0;
    dst = address(sc);
    
    emit(sv_cpu_iJMP);

    if(isgood(sc)) 
        emit32(dst);

    good(sc);
}

void instr_rel(success_t* sc) {
    sv_word dst = 0;
    dst = address(sc);
    
    emit(sv_cpu_iREL);

    if(isgood(sc)) 
        emit32(dst);

    good(sc);
}

void instr_nrel(success_t* sc) {
    sv_word dst = 0;
    dst = address(sc);
    
    emit(sv_cpu_iNREL);

    if(isgood(sc)) 
        emit32(dst);

    good(sc);
}

void instr_and(success_t* sc) {
    emit(sv_cpu_iAND);
}

void instr_or(success_t* sc) {
    emit(sv_cpu_iOR);
}

void instr_xor(success_t* sc) {
    emit(sv_cpu_iXOR);
}

void instr_not(success_t* sc) {
    emit(sv_cpu_iNOT);
}

void instr_lsh(success_t* sc) {
    emit(sv_cpu_iLSH);
}

void instr_rsh(success_t* sc) {
    emit(sv_cpu_iRSH);
}

void instr_dup(success_t* sc) {
    emit(sv_cpu_iDUP);
}
void instr_swap(success_t* sc) {
    emit(sv_cpu_iSWAP);
}

void instr_eq(success_t* sc) {
    emit(sv_cpu_iEQ);
}

void instr_neq(success_t* sc) {
    emit(sv_cpu_iNEQ);
}

void instr_gr(success_t* sc) {
    emit(sv_cpu_iGR);
}

void instr_ls(success_t* sc) {
    emit(sv_cpu_iLS);
}

void instr_inc(success_t* sc) {
    emit(sv_cpu_iINC);
}

void instr_dec(success_t* sc) {
    emit(sv_cpu_iDEC);
}

void instr_call(success_t* sc) {
    sv_word dst = 0;
    dst = address(sc);
    
    emit(sv_cpu_iCALL);

    if(isgood(sc)) 
        emit32(dst);

    good(sc);
}

void instr_ret(success_t* sc) {
    emit(sv_cpu_iRET);
}

void instr(success_t* sc) {
    char* inst = identf_instr(sc);
    if(isbad(sc))
        goto error;

    char* instrs[] = {
        "nop", "int", "push", "pop", "add", "sub",
        "mul", "div", "put", "put8", "put32", "deref",
        "jmp", "rel", "nrel", "and", "or", "xor", "not",
        "lsh", "rsh", "dup", "swap", "eq", "neq", "gr",
        "ls", "inc", "dec", "call", "ret"
    };

    void(*instrs_fn[])(success_t*) = { 
        instr_nop, instr_int, instr_push, instr_pop,
        instr_add, instr_sub, instr_mul, instr_div,
        instr_put, instr_put8, instr_put32,
        instr_deref, instr_jmp, instr_rel, instr_nrel,
        instr_and, instr_or, instr_xor, instr_not,
        instr_lsh, instr_rsh, instr_dup, instr_swap,
        instr_eq, instr_neq, instr_gr, instr_ls,
        instr_inc, instr_dec, instr_call, instr_ret
    };

    for(int i = 0; i < sizeof(instrs) / sizeof(instrs[0]); ++i) {
        if(!strcmp(instrs[i], inst)) {
            instrs_fn[i](sc);
            if(isbad(sc))
                goto error;
            free(inst);
            return;
        }
    }

error:
    if(inst) free(inst);
    bad(sc, "not an instruction");
}

