#ifndef STREAM_H
#define STREAM_H

#include "def.h"

output oopen(const char* file);

void oclose(output o);

void emit(sv_byte b);

void emit32(sv_word b);

stream_t* get_stream(void);

stream_t sstart(stream_t s);

stream_t at(int c); 
char incr(int c);
char peek(void);
char next(void);
char eat(void);
char back(void);
void backby(size_t n); 
size_t left(void); 
int empty(void); 

stream_t stream_file(const char* file); 

#endif
