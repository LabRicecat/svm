#include "position.h"
#include "stream.h"

struct position cpos(void) {
    struct position pos = {1, 0};
    stream_t start = sstart(NULL);
    while(start != at(0)) {
        if(*start == '\n')
            pos.column = 1, ++pos.line;
        else 
            ++pos.column;

        ++start;
    }
    
    return pos;
}
