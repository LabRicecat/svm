#ifndef SUCCESS_H
#define SUCCESS_H

#include "def.h"

success_t mksuccess(void);
void      rmsuccess(success_t* sc);
success_t* good(success_t* s);
int        isgood(success_t* s);
success_t* bad(success_t* s, char* msg);
int        isbad(success_t* s);

#endif
