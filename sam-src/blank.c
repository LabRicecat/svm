#include "blank.h"
#include "stream.h"
#include "success.h"
#include <ctype.h>

void whitespace(success_t* sc) {
    int c = 0;
    while(isspace(peek()) && peek() != '\n')
        eat(), ++c;

    if(c) return;

    bad(sc, "no whitespace");
}

void newline(success_t* sc) {
    int c = 0;
    while(peek() == '\n')
        eat(), ++c;

    if(c) return;
        
    bad(sc, "no newline");
}
