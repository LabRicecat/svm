#ifndef BLANK_H
#define BLANK_H

#include "def.h"

void whitespace(success_t* sc);

void newline(success_t* sc);

#endif
