#include "value.h"
#include "stream.h"
#include "success.h"
#include "../svm-src/devices/CPU.h"

#include <memory.h>
#include <stdlib.h>
#include <ctype.h>

char* identf(success_t* sc) {
    char* s;
    size_t ss = 0;
    
    while(isalpha(peek())) 
        ++ss, eat();

    if(!ss) {
        bad(sc, "not an identifier");
        return NULL;
    }
    
    s = malloc(ss + 1);
    s[ss] = 0;
    memcpy(s, at(-ss), ss);

    return s;
}

char* identf_instr(success_t* sc) {
    char* s;
    size_t ss = 0;
    
    while((isalpha(peek()) && ss == 0) || (ss > 0 && isalnum(peek()))) 
        ++ss, eat();

    if(!ss) {
        bad(sc, "not an identifier");
        return NULL;
    }
    
    s = malloc(ss + 1);
    s[ss] = 0;
    memcpy(s, at(-ss), ss);

    return s;
}

sv_word decimal(success_t* sc) {
    sv_word i = 0;
    size_t ss = 0;
    
    while(isdigit(peek())) 
        i = (eat() - '0') + i * 10, ++ss;
    
    if(!ss) 
        bad(sc, "not a decimal number");

    return i;
}

int ishex(int x) {
    return isdigit(x) || ( tolower(x) <= 'f' && tolower(x) >= 'a' );
}

sv_word hex(success_t* sc) {
    if(peek() != 'x')
        goto error;
    eat();

    sv_word i = 0;
    size_t ss = 0;

    while(ishex(peek())) 
        i = ( isdigit(peek()) ? (eat() - '0') : (tolower(eat()) - 'a' + 10) ) + i * 16, ++ss;
    
    if(!ss) 
        goto error;

    return i;
error:
    bad(sc, "not a hex number");
    return 0;
}

int binary(success_t* sc) {
    if(peek() != 'b')
        goto error;
    eat();

    sv_word i = 0;
    size_t ss = 0;

    while(peek() == '0' || peek() == '1') 
        i = (eat() == '1') + i * 2, ++ss;
    
    if(!ss) 
        goto error;

    return i;
error:
    bad(sc, "not a binary number");
    return 0;
}

sv_word number(success_t* sc) {
    if(peek() == 'x')
        return hex(sc);
    else if(peek() == 'b')
        return binary(sc);
    else 
        return decimal(sc);
}

sv_fd regist(success_t* sc) {
    char* name = identf(sc);
    if(isbad(sc)) 
        goto error;
    
    if(strlen(name) != 2)
        goto error;

    if(*name != 'R' || *name != 'r')
        goto error;

    if(!isalpha(name[1]))
        goto error;

    char r = tolower(name[1]) - 'a';
    free(name);

    return sv_cpu_reg(r);

error:
    if(name) {
        backby(strlen(name));
        free(name);
    }
    bad(sc, "not a register");
    return 0;
}

sv_fd devicedesc(success_t* sc) {
    // ram0
    sv_word offset;
    char* device;

    device = identf(sc);
    if(isbad(sc)) {
        bad(sc, "error with identifier");
        goto error;
    }

    good(sc);
    offset = decimal(sc);

    if(isbad(sc))
        offset = 0;

    good(sc);

    if(!strcmp(device, "ram")) return free(device), sv_mkfd(0, offset);
    if(!strcmp(device, "drv")) return free(device), sv_mkfd(1, offset);
    if(!strcmp(device, "cpu")) return free(device), sv_mkfd(2, offset);
    if(!strcmp(device, "dis")) return free(device), sv_mkfd(3, offset);
    if(!strcmp(device, "rom")) return free(device), sv_mkfd(4, offset);
    if(!strcmp(device, "key")) return free(device), sv_mkfd(5, offset);

error:
    if(device) free(device);
    return 0;
}

sv_fd address(success_t* sc) {
    sv_fd addr;

    addr = number(sc);
    if(isgood(sc))
        return addr;
    good(sc);

    addr = regist(sc);
    if(isgood(sc))
        return addr;
    good(sc);

    addr = devicedesc(sc);
    if(isgood(sc))
        return addr;
    good(sc);

    // add more here

    bad(sc, "not a address");
    return 0;
}

