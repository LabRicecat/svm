#ifndef INSTRUCTION_H
#define INSTRUCTION_H
#include "def.h"

void instr_nop(success_t* sc);
void instr_int(success_t* sc);
void instr_push(success_t* sc);
void instr_pop(success_t* sc);
void instr_add(success_t* sc);
void instr_sub(success_t* sc);
void instr_mul(success_t* sc);
void instr_div(success_t* sc); 
void instr_put(success_t* sc); 
void instr_put8(success_t* sc); 
void instr_put32(success_t* sc); 
void instr_deref(success_t* sc); 
void instr_jmp(success_t* sc); 
void instr_rel(success_t* sc); 
void instr_nrel(success_t* sc); 
void instr_and(success_t* sc); 
void instr_or(success_t* sc); 
void instr_xor(success_t* sc); 
void instr_not(success_t* sc); 
void instr_lsh(success_t* sc); 
void instr_rsh(success_t* sc); 
void instr_dup(success_t* sc); 
void instr_swap(success_t* sc); 
void instr_eq(success_t* sc); 
void instr_neq(success_t* sc); 
void instr_gr(success_t* sc); 
void instr_ls(success_t* sc); 
void instr_inc(success_t* sc); 
void instr_dec(success_t* sc); 
void instr_call(success_t* sc); 
void instr_ret(success_t* sc); 
void instr(success_t* sc);

#endif
