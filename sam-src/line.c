#include "line.h"
#include "success.h"
#include "stream.h"
#include "blank.h"
#include "instruction.h"

void line(success_t* sc) {
    success_t ws_sc = mksuccess();
    good(&ws_sc);
    success_t nl_sc = mksuccess();
    good(&nl_sc);
    do {
        whitespace(&ws_sc);
        newline(&nl_sc);
    } while(isgood(&nl_sc) && isgood(&ws_sc)); 

    rmsuccess(&ws_sc);
    rmsuccess(&nl_sc);

    instr(sc);
    if(isbad(sc)) 
        goto error;
    
    if(empty())
        return;
   
    whitespace(NULL);
    newline(sc);
    if(isbad(sc))
        goto error;

    return;
error: 
    bad(sc, "not a line");
}
