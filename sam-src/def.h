#ifndef DEF_H
#define DEF_H

#include <stdio.h>
#include "../svm-src/bus.h"

#define GOOD 1
#define BAD  0

#define ERROR_TRACE_SIZE 100

struct position {
    unsigned int line;
    unsigned int column;
};

struct error_t {
    char* msg;
    struct position pos; 
};

typedef struct success {
    int status;
    size_t errors;
    struct error_t* errorv;
} success_t;

typedef FILE* output;
typedef char* stream_t;

#endif
