#ifndef SV_DEVICES_H
#define SV_DEVICES_H

#include "bus.h"

#define SV_DEVICE_DOWN 0
#define SV_DEVICE_UP   1

struct sv_device;
typedef int(*sv_dstartup)(struct sv_device*);
typedef int(*sv_dshutdown)(struct sv_device*);
typedef void(*sv_dcycle)(struct sv_device*);

struct sv_device {
    int                 up;
    sv_dstartup         startup;
    sv_dshutdown        shutdown;
    sv_dcycle           cycle;
    struct sv_bentry    entry;
};

struct sv_device sv_newdevice(sv_bread, sv_bwrite, sv_dstartup, sv_dshutdown, sv_dcycle);

struct sv_device* sv_ddown(struct sv_device*);
struct sv_device* sv_dup(struct sv_device*);

sv_blk sv_dblock(struct sv_device*);

sv_fd sv_dwrite(struct sv_device*, sv_rel, sv_byte);
sv_byte sv_dread(struct sv_device*, sv_rel);

sv_fd sv_open(struct sv_device*);

#endif
