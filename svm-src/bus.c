#include "bus.h"
#include <limits.h>
#include <stdlib.h>

sv_fd sv_mkfd(sv_blk blk, sv_rel rel) {
    sv_fd fd;
    fd = blk << (sizeof(fd)*CHAR_BIT - sizeof(blk)*CHAR_BIT);
    fd += rel;
    return fd;
}

sv_blk sv_fdblk(sv_fd fd) {
    return fd >> (sizeof(sv_fd) - sizeof(sv_blk))*CHAR_BIT;
}

sv_rel sv_fdrel(sv_fd fd) {
    return fd << sizeof(sv_blk)*CHAR_BIT >> sizeof(sv_blk)*CHAR_BIT;
}

sv_fd sv_fdsuc(sv_fd fd, sv_byte byte) {
    return fd + byte;
}

sv_fd sv_fdseek(sv_fd fd, sv_rel rel) {
    return sv_mkfd(sv_fdblk(fd), rel);
}

struct sv_bentry sv_bentry_register(sv_bread read, sv_bwrite write) {
    static sv_blk blk = -1;
    blk++;
    return sv_bentries()[blk] = (struct sv_bentry){ blk, read, write };
}

struct sv_bentry* sv_bentries(void) {
    static struct sv_bentry entries[SV_BENTRIES_MAX];
    return entries;
}

sv_fd sv_write(sv_fd fd, sv_byte byte) {
    return sv_fdsuc(sv_bentries()[sv_fdblk(fd)].write(fd, byte), 1);
}

sv_fd sv_write32(sv_fd fd, sv_word word) {
    for(size_t i = sizeof(word); i; --i)
        fd = sv_write(fd, (word >> ((i-1)*CHAR_BIT)) & 0xff);
    return fd;
}

sv_byte sv_read(sv_fd fd) {
    return sv_bentries()[sv_fdblk(fd)].read(fd);
}
