#include "devices.h"

struct sv_device sv_newdevice(sv_bread read, sv_bwrite write, sv_dstartup startup, sv_dshutdown shutdown, sv_dcycle cycle) {
    return (struct sv_device){ SV_DEVICE_DOWN, startup, shutdown, cycle, sv_bentry_register(read, write) };
}

struct sv_device* sv_ddown(struct sv_device* dv) {
    dv->up = dv->shutdown(dv) == 0 ? SV_DEVICE_DOWN : SV_DEVICE_UP;
    return dv;
}

struct sv_device* sv_dup(struct sv_device* dv) {
    dv->up = dv->startup(dv) == 0 ? SV_DEVICE_UP : SV_DEVICE_DOWN;
    return dv;
}

sv_blk sv_dblock(struct sv_device* dv) {
    return dv->entry.block;
}

sv_fd sv_dwrite(struct sv_device* dv, sv_rel rel, sv_byte byte) {
    return sv_write(sv_mkfd(sv_dblock(dv), rel), byte);
}

sv_byte sv_dread(struct sv_device* dv, sv_rel rel) {
    return sv_read(sv_mkfd(sv_dblock(dv), rel));
}

sv_fd sv_open(struct sv_device* dv) {
    return sv_mkfd(sv_dblock(dv), 0);
}
