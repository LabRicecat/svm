#ifndef SV_BLOCKMAPPER_H
#define SV_BLOCKMAPPER_H

enum {
    SV_FIXEDMAPPED_RANGE, 

    SV_FIXEDMAPPED_RAM,
    SV_FIXEDMAPPED_CPU,
    SV_FIXEDMAPPED_DRIVE,
    SV_FIXEDMAPPED_DISPLAY,
    SV_FIXEDMAPPED_ROM,
    SV_FIXEDMAPPED_KEYBOARD,

    SV_FIXEDMAPPED_SIZE, 
};

void sv_blockmapper(void);

#endif
