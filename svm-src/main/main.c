#include "../BIOS.h"

int main(void) {
    sv_bios_initialize();
    while(sv_bios_awake())
        sv_bios_cycle(); 
    sv_bios_shutdown();
}
