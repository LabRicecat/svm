#include "BIOS.h"
#include "devices/CPU.h"
#include "devices.h"
#include "manager.h"
#include "blockmapper.h"
#include <stdlib.h>

int bios_awake = 0;

int sv_bios_awake(void) {
    return bios_awake;
}

void sv_bios_startup(void) {
    bios_awake = 1;
    size_t p = 0;
    struct sv_device* dv;
    while((dv = sv_bios_autostart(NULL)[p++]))
        if(!dv->up) dv->up = !dv->startup(dv);

    sv_blockmapper();
}

void sv_bios_shutdown(void) {
    size_t p = 0;
    struct sv_device* dv;
    while((dv = sv_bios_autostart(NULL)[p++]))
        if(dv->up) dv->up = dv->shutdown(dv);

    bios_awake = 0;
}

void sv_bios_cycle(void) {
    size_t p = 0;
    struct sv_device* dv;
    while((dv = sv_bios_autocycle(NULL)[p++]))
        if(dv->up) dv->cycle(dv);
}

void sv_bios_fire(enum sv_bios_interupt interrupt) {
    // TODO
    switch(interrupt) {
        case sv_bios_iSHUTDOWN:
            sv_bios_shutdown();
        break;
        case sv_bios_iSTKDUMP:
            sv_cpu_stackdump();
        break;
    }
}

struct sv_device** sv_bios_autostart(struct sv_device* dv) {
    static struct sv_device* devices[SV_BIOS_AUTOSTART_MAX+1] = {0};
    size_t p = 0;
    if(!dv)
        return devices;

    while(p < SV_BIOS_AUTOSTART_MAX && devices[p] != 0)
        p++;

    if(p != SV_BIOS_AUTOCYCLE_MAX)
        devices[p] = dv;
    return devices;
}

struct sv_device** sv_bios_autocycle(struct sv_device* dv) {
    static struct sv_device* devices[SV_BIOS_AUTOCYCLE_MAX+1] = {0};
    size_t p = 0;
    if(!dv)
        return devices;

    while(p < SV_BIOS_AUTOSTART_MAX && devices[p] != 0)
        p++;

    if(p != SV_BIOS_AUTOCYCLE_MAX)
        devices[p] = dv;
    return devices;
}

void sv_bios_initialize(void) {
    sv_device_manage();
    sv_bios_startup();
}
