#include "RAM.h"
#include <stdlib.h>

sv_byte* RAM_SPACE = NULL;

sv_fd sv_ram_write(sv_fd fd, sv_byte byte) {
    RAM_SPACE[sv_fdrel(fd)] = byte;
    return fd;
}

sv_byte sv_ram_read(sv_fd fd) {
    return RAM_SPACE[sv_fdrel(fd)];
}

int sv_ram_startup(struct sv_device* ram) {
    RAM_SPACE = calloc(sizeof(sv_byte), SV_DEVICE_RAM_SPACE);

    return !(RAM_SPACE != NULL);
}

int sv_ram_shutdown(struct sv_device* ram) {
    free(RAM_SPACE);
    return 0;
}

struct sv_device* sv_get_ram(void) {
    static struct sv_device* ram = NULL;
    if(ram == NULL) {
        struct sv_device dv = sv_newdevice(
            sv_ram_read,
            sv_ram_write,
            sv_ram_startup,
            sv_ram_shutdown,
            NULL
        );
        ram = malloc(sizeof(dv));
        *ram = dv;
    }
    
    return ram;
}
