#ifndef SV_DEVICE_ROM_H 
#define SV_DEVICE_ROM_H

#include "../devices.h"
#include <limits.h>

#define SV_DEVICE_ROM_SPACE 1000
#define SV_DEVICE_ROM_MBR_SPACE (sizeof(sv_byte)*CHAR_BIT)

struct sv_device* sv_get_rom(void); 

#endif
