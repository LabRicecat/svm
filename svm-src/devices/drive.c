#include "drive.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <string.h>

FILE* DRIVE_FILE = NULL;

sv_fd sv_drive_write(sv_fd fd, sv_byte byte) {
    fseek(DRIVE_FILE, sv_fdrel(fd), SEEK_SET);
    fputc(byte, DRIVE_FILE);
    return sv_fdsuc(fd, 1);
}

sv_byte sv_drive_read(sv_fd fd) {
    fseek(DRIVE_FILE, sv_fdrel(fd), SEEK_SET);
    sv_byte byte = fgetc(DRIVE_FILE);
    fseek(DRIVE_FILE, sv_fdrel(fd), SEEK_SET);
    return byte;
}

int sv_drive_startup(struct sv_device* dv) {
    DRIVE_FILE = fopen(SV_DRIVE_FILE, "r+");
    
    if(DRIVE_FILE == NULL) {
        DRIVE_FILE = fopen(SV_DRIVE_FILE, "w");
        for(size_t i = 0; i < pow(2, (sizeof(sv_fd) - sizeof(sv_blk)) * CHAR_BIT); ++i)
            fputc(0, DRIVE_FILE);
        fclose(DRIVE_FILE);

        DRIVE_FILE = fopen(SV_DRIVE_FILE, "r+");
    }
    return DRIVE_FILE == NULL;
}

int sv_drive_shutdown(struct sv_device* dv) {
    fclose(DRIVE_FILE);
    return 0;
}

void sv_drive_cycle(struct sv_device* dv) {
    return;
}

struct sv_device* sv_get_drive(void) {
    static struct sv_device* drive = NULL;
    if(drive == NULL) {
        struct sv_device dv = sv_newdevice(
            sv_drive_read,
            sv_drive_write,
            sv_drive_startup,
            sv_drive_shutdown,
            sv_drive_cycle
        );

        drive = malloc(sizeof(dv));
        *drive = dv;
    }

    return drive;
}
