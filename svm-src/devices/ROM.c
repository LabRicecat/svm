#include "ROM.h"
#include "CPU.h"
#include <stdlib.h>
#include <memory.h>

sv_byte* ROM_SPACE = NULL;

sv_fd sv_rom_write(sv_fd fd, sv_byte byte) {
    return fd;
}

sv_byte sv_rom_read(sv_fd fd) {
    return ROM_SPACE[sv_fdrel(fd)];
}

#define RA 2,0,0,0
#define RB 2,0,0,4
#define RC 2,0,0,8

int sv_rom_startup(struct sv_device* ram) {
    sv_byte rom_prog[] = {
        sv_cpu_iPUSH, 0,0,0,0,
        sv_cpu_iPUSH, 1,0,0,0,
        sv_cpu_iJMP,

        sv_cpu_iPUSH, 1,0,0,0, // first byte of drive 
        sv_cpu_iDEREF,
        sv_cpu_iPUSH, 1,0,0,1, // second byte 
        sv_cpu_iDEREF,

        sv_cpu_iADD, // add together
            
        sv_cpu_iPUSH, 3,0,0,0, // put on screen 
        sv_cpu_iPUT8,

        sv_cpu_iPUSH, 5,0,0,0, // keyboard state 
        sv_cpu_iDEREF, 

        sv_cpu_iPUSH, 0,0,0,97,
        sv_cpu_iEQ, // equal to `a`
        sv_cpu_iPUSH, 0,0,0,17,
        sv_cpu_iNREL, // jump back

        sv_cpu_iPUSH, 0,0,0,0,
        sv_cpu_iINTER, // exit
    };
    ROM_SPACE = calloc(sizeof(sv_byte), SV_DEVICE_ROM_SPACE);

    memcpy(ROM_SPACE, rom_prog, sizeof(rom_prog)/sizeof(sv_byte));

    return ROM_SPACE != NULL;
}

int sv_rom_shutdown(struct sv_device* ram) {
    free(ROM_SPACE);
    return 0;
}

struct sv_device* sv_get_rom(void) {
    static struct sv_device* ram = NULL;
    if(ram == NULL) {
        struct sv_device dv = sv_newdevice(
            sv_rom_read,
            sv_rom_write,
            sv_rom_startup,
            sv_rom_shutdown,
            NULL
        );
        ram = malloc(sizeof(dv));
        *ram = dv;
    }
    
    return ram;
}
