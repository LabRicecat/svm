#include "display.h"
#include <stdlib.h>
#include <ncurses.h>

sv_fd sv_display_write(sv_fd fd, sv_byte byte) {
    mvwprintw(stdscr, sv_fdrel(fd) / SV_DISPLAY_RESOLUTION_X, sv_fdrel(fd) % SV_DISPLAY_RESOLUTION_X, "%c", byte);
    refresh();
    return sv_fdsuc(fd, 1);
}

sv_byte sv_display_read(sv_fd fd) {
    move(sv_fdrel(fd) / SV_DISPLAY_RESOLUTION_X, sv_fdrel(fd) % SV_DISPLAY_RESOLUTION_X);
    return inch();
}

int sv_display_startup(struct sv_device* display) {
    initscr();
    nodelay(stdscr, TRUE);
    noecho();
    keypad(stdscr, TRUE);
    return 0;
}

int sv_display_shutdown(struct sv_device* display) {
    endwin();
    return 0;
}

struct sv_device* sv_get_display(void) {
    static struct sv_device* display = NULL;
    if(display == NULL) {
        struct sv_device dv = sv_newdevice(
            sv_display_read,
            sv_display_write,
            sv_display_startup,
            sv_display_shutdown,
            NULL
        );

        display = malloc(sizeof(dv));
        *display = dv;
    }

    return display;
}
