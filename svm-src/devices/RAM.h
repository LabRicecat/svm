#ifndef SV_DEVICE_RAM_H
#define SV_DEVICE_RAM_H

#include "../devices.h"
#include <limits.h>

#define SV_DEVICE_RAM_SPACE ((sizeof(sv_fd) - sizeof(sv_blk)) * CHAR_BIT)

struct sv_device* sv_get_ram(void); 

#endif
