#ifndef SV_DEVICE_DRIVE_H
#define SV_DEVICE_DRIVE_H 

#ifndef SV_DRIVE_FILE
#define SV_DRIVE_FILE "drive.svm"
#endif

#include "../devices.h"

struct sv_device* sv_get_drive(void);

#endif
