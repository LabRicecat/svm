#include "keyboard.h"
#include <stdlib.h>
#include <ncurses.h>

sv_byte* KEYBOARD_SPACE = NULL;

sv_fd sv_keyboard_write(sv_fd fd, sv_byte byte) {
    KEYBOARD_SPACE[sv_fdrel(fd)] = byte;
    return fd;
}

sv_byte sv_keyboard_read(sv_fd fd) {
    return KEYBOARD_SPACE[sv_fdrel(fd)];
}

int sv_keyboard_startup(struct sv_device* ram) {
    KEYBOARD_SPACE = calloc(sizeof(sv_byte), SV_DEVICE_KEYBOARD_SPACE);

    return !(KEYBOARD_SPACE != NULL);
}

int sv_keyboard_shutdown(struct sv_device* ram) {
    free(KEYBOARD_SPACE);
    return 0;
}

void sv_keyboard_cycle(struct sv_device* keyboard) {
    int c;
    if((c = getch()) != EOF)
        KEYBOARD_SPACE[0] = c;
}

struct sv_device* sv_get_keyboard(void) {
    static struct sv_device* keyboard = NULL;
    if(keyboard == NULL) {
        struct sv_device dv = sv_newdevice(
            sv_keyboard_read,
            sv_keyboard_write,
            sv_keyboard_startup,
            sv_keyboard_shutdown,
            sv_keyboard_cycle
        );
        keyboard = malloc(sizeof(dv));
        *keyboard = dv;
    }
    
    return keyboard;
}
