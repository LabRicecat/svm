#ifndef SV_DEVICE_KEYBOARD_H
#define SV_DEVICE_KEYBOARD_H

#include "../devices.h"

#define SV_DEVICE_KEYBOARD_SPACE 1

struct sv_device* sv_get_keyboard(void); 

#endif
