#ifndef SV_DEVICE_DISPLAY_H
#define SV_DEVICE_DISPLAY_H

#include "../devices.h"

#define SV_DISPLAY_RESOLUTION_X 100
#define SV_DISPLAY_RESOLUTION_Y 100

struct sv_device* sv_get_display(void);

#endif
