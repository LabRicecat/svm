#include "CPU.h"
#include "ROM.h"
#include "../BIOS.h"
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

sv_fd sv_cpu_reg(enum sv_cpu_register reg) {
    return sv_mkfd(2, reg * sizeof(sv_word));
}

sv_word sv_cpu_registers[END_OF_REGISTERS] = {0}; 

sv_word sv_cpu_stack[SV_DEVICE_STACK_SIZE] = {0};
sv_word sv_cpu_sptr = 0;

sv_fd sv_cpu_callstack[SV_DEVICE_CALLSTACK_SIZE] = {0};
sv_word sv_cpu_csptr = 0;

void sv_cpu_stackdump(void) {
    const char* regs[] = { 
        "RA", "RB", "RC", "RD", 
        "RE", "RF", "RG"
    };
    FILE* log = fopen(SV_DEVICE_CPU_STACKDUMP_DST, "w+");
    if(!log) return;

    fputs("Stackdump:\n", log);
    for(size_t i = 0; i < sv_cpu_sptr; ++i) {
        fprintf(log, "%3d | %10d %#10x\n", (int)i, sv_cpu_stack[i], sv_cpu_stack[i]);
    }

    fputs("\nRegisters:\n", log);
    for(size_t i = 0; i < END_OF_REGISTERS; ++i) {
        fprintf(log, "%3d | %10d %#10x\n", (int)i, sv_cpu_registers[i], sv_cpu_registers[i]);
    }

    fclose(log);
}

sv_word sv_cpu_push(sv_word wd) {
    return sv_cpu_stack[sv_cpu_sptr++] = wd;
}

sv_word sv_cpu_pop(void) {
    return sv_cpu_stack[--sv_cpu_sptr];
}

sv_fd sv_cpu_cpush(sv_fd fd) {
    return sv_cpu_callstack[sv_cpu_csptr++] = fd;
}

sv_fd sv_cpu_cpop(void) {
    return sv_cpu_callstack[--sv_cpu_csptr];
}

sv_fd sv_cpu_write(sv_fd fd, sv_byte byte) {
    sv_byte reg = sv_fdrel(fd) / sizeof(sv_word);
    sv_byte pos = sizeof(sv_word) - sv_fdrel(fd) % sizeof(sv_word) - 1;

    for(size_t i = 0; i < sizeof(sv_byte)*CHAR_BIT; ++i)
        sv_cpu_registers[reg] |= (byte & (1 << i)) << ( pos * CHAR_BIT );

    return fd;
}

sv_byte sv_cpu_read(sv_fd fd) {
    return sv_cpu_registers[sv_fdrel(fd) / sizeof(sv_word)] >> (sv_fdrel(fd) % sizeof(sv_word))*CHAR_BIT;
}

int sv_cpu_startup(struct sv_device* dv) {
    sv_cpu_registers[IC] = sv_open(sv_get_rom());
    return 0;
}

int sv_cpu_shutdown(struct sv_device* dv) {
    return 0;
}

sv_byte get8(void) {
    return sv_read(sv_cpu_registers[IC] = sv_fdsuc(sv_cpu_registers[IC], 1));
}

sv_word get32(void) {
    sv_word wd = 0;
    for(int i = sizeof(sv_word); i; --i)
        wd += get8() << (i - 1)*CHAR_BIT;

    return wd;
}

void sv_cpu_put(sv_word dst, sv_word val, sv_word n) {
    for(size_t i = 0; i < n / (sizeof(sv_byte)*CHAR_BIT); ++i) {
        sv_write(sv_fdsuc(dst, i), (val >> (n - CHAR_BIT*(i + 1))) & 0xff);
    }
}

void sv_cpu_cycle(struct sv_device* dv) {
    switch(sv_read(sv_cpu_registers[IC])) {
        case sv_cpu_iNOP:
            // ... 
        break;
        case sv_cpu_iINTER:
            sv_bios_fire(sv_cpu_pop());
        break;
        case sv_cpu_iPUSH:
            sv_cpu_push(get32());
        break;
        case sv_cpu_iPOP:
            sv_cpu_pop();
        break;
        case sv_cpu_iADD:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] + sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iSUB:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] - sv_cpu_registers[RHS]);     
        break;
        case sv_cpu_iMUL:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] * sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iDIV:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] / sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iPUT:
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_put(sv_cpu_registers[LHS], sv_cpu_registers[RHS], sv_cpu_pop());
        break;
        case sv_cpu_iPUT8:
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_put(sv_cpu_registers[LHS], sv_cpu_registers[RHS], 8);
        break;
        case sv_cpu_iPUT32:
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_put(sv_cpu_registers[LHS], sv_cpu_registers[RHS], 32);
        break;
        case sv_cpu_iDEREF: {
            sv_word fd = 0;
            sv_cpu_registers[LHS] = sv_cpu_pop();
            for(size_t i = 0; i < sizeof(sv_fd); ++i)
                fd += sv_read(sv_fdsuc(sv_cpu_registers[LHS], i)) << i*CHAR_BIT;
            sv_cpu_push(fd);
        }
        break;
        case sv_cpu_iJMP:
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_registers[RHS] = sv_cpu_pop();
            if(sv_cpu_registers[RHS] == 0)
                sv_cpu_registers[IC] = sv_cpu_registers[LHS] - 1;
        break;
        case sv_cpu_iREL:
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_registers[RHS] = sv_cpu_pop();
            if(sv_cpu_registers[RHS] == 0)
                sv_cpu_registers[IC] += sv_cpu_registers[LHS] - 1;
        break;
        case sv_cpu_iNREL: 
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_registers[RHS] = sv_cpu_pop();
            if(sv_cpu_registers[RHS] == 0)
                sv_cpu_registers[IC] -= sv_cpu_registers[LHS] + 1;

        break;
        case sv_cpu_iAND:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] & sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iOR:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] & sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iXOR:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] ^ sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iNOT:
            sv_cpu_push(~sv_cpu_pop());
        break;
        case sv_cpu_iLSH:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] << sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iRSH:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] >> sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iDUP:
            sv_cpu_push(sv_cpu_push(sv_cpu_pop()));
        break;
        case sv_cpu_iSWAP:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[RHS]);
            sv_cpu_push(sv_cpu_registers[LHS]);
        break;
        case sv_cpu_iEQ:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] == sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iNEQ:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] != sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iGR:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] > sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iLS:
            sv_cpu_registers[RHS] = sv_cpu_pop();
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_push(sv_cpu_registers[LHS] < sv_cpu_registers[RHS]);
        break;
        case sv_cpu_iINC:
            sv_cpu_push(sv_cpu_pop() + 1);
        break;
        case sv_cpu_iDEC:
            sv_cpu_push(sv_cpu_pop() - 1);
        break;
        case sv_cpu_iCALL:
            sv_cpu_registers[LHS] = sv_cpu_pop();
            sv_cpu_registers[RHS] = sv_cpu_pop();
            if(sv_cpu_registers[RHS] == 0) {
                sv_cpu_registers[IC] = sv_cpu_registers[LHS] - 1;
                sv_cpu_cpush(sv_cpu_registers[LHS]);
            }
        break;
        case sv_cpu_iRET:
            sv_cpu_registers[IC] = sv_cpu_cpop() - 1;
        break;
        case sv_cpu_iPOS:
            sv_cpu_push(sv_cpu_registers[IC]);
        break;
    };
    sv_cpu_registers[IC] = sv_fdsuc(sv_cpu_registers[IC], 1);
}

struct sv_device* sv_get_cpu(void) {
    static struct sv_device* cpu = NULL;
    if(cpu == NULL) {
        struct sv_device dv = sv_newdevice(
            sv_cpu_read,
            sv_cpu_write,
            sv_cpu_startup,
            sv_cpu_shutdown,
            sv_cpu_cycle
        );
        cpu = malloc(sizeof(dv));
        *cpu = dv;
    }
    
    return cpu;
}
