#ifndef SV_DEVICE_CPU_H
#define SV_DEVICE_CPU_H

#include "../devices.h"
#include <limits.h>

#define SV_DEVICE_STACK_SIZE ((sizeof(sv_fd) - sizeof(sv_blk))*CHAR_BIT)
#define SV_DEVICE_CALLSTACK_SIZE 1000
#define SV_DEVICE_CPU_STACKDUMP_DST "stackdump"

enum sv_cpu_register { // max: 256 
    RA, RB,
    RC, RD,
    RE, RF,
    RG, RH,
    RI, RJ,
    RK, RL,
    RM, RN,
    RO, RP,
    RQ, RR,
    RS, RT,
    RU, RV, 
    RW, RX,
    RY, RZ,

    IC,
   
    LHS, RHS,
    END_OF_REGISTERS
};

sv_fd sv_cpu_reg(enum sv_cpu_register);
void sv_cpu_stackdump(void);

enum sv_cpu_instr {
    sv_cpu_iNOP,
    sv_cpu_iINTER,
    sv_cpu_iPUSH,
    sv_cpu_iPOP,
    sv_cpu_iADD,
    sv_cpu_iSUB,
    sv_cpu_iMUL,
    sv_cpu_iDIV,
    sv_cpu_iPUT, 
    sv_cpu_iPUT8, 
    sv_cpu_iPUT32,
    sv_cpu_iDEREF,
    sv_cpu_iJMP,
    sv_cpu_iREL,
    sv_cpu_iNREL,
    sv_cpu_iAND,
    sv_cpu_iOR,
    sv_cpu_iXOR,
    sv_cpu_iNOT,
    sv_cpu_iLSH,
    sv_cpu_iRSH, 
    sv_cpu_iDUP,
    sv_cpu_iSWAP,
    sv_cpu_iEQ,
    sv_cpu_iNEQ,
    sv_cpu_iGR,
    sv_cpu_iLS,
    sv_cpu_iINC,
    sv_cpu_iDEC,
    sv_cpu_iCALL,
    sv_cpu_iRET,
    sv_cpu_iPOS,
};

struct sv_device* sv_get_cpu(void);

#endif
