#include "devices/RAM.h"
#include "devices/drive.h"
#include "devices/CPU.h"
#include "devices/display.h"
#include "devices/ROM.h"
#include "devices/keyboard.h"
#include "BIOS.h"

void sv_device_manage(void) {
    struct sv_device* ram = sv_get_ram();
    struct sv_device* drive = sv_get_drive();
    struct sv_device* cpu = sv_get_cpu(); 
    struct sv_device* display = sv_get_display();
    struct sv_device* rom = sv_get_rom();
    struct sv_device* keyboard = sv_get_keyboard();

    sv_bios_autostart(rom);
    sv_bios_autostart(drive);
    sv_bios_autostart(ram);
    sv_bios_autostart(cpu);
    sv_bios_autostart(display);
    sv_bios_autostart(keyboard);

    sv_bios_autocycle(keyboard);
    sv_bios_autocycle(cpu);
}
