#ifndef SV_BIOS_H 
#define SV_BIOS_H

#include "devices.h"

#define SV_BIOS_AUTOSTART_MAX 10
#define SV_BIOS_AUTOCYCLE_MAX 10

enum sv_bios_interupt {
    sv_bios_iSHUTDOWN,
    sv_bios_iSTKDUMP
};

int sv_bios_awake(void);

void sv_bios_initialize(void);

void sv_bios_startup(void);
void sv_bios_shutdown(void);
void sv_bios_cycle(void);

void sv_bios_fire(enum sv_bios_interupt);

struct sv_device** sv_bios_autostart(struct sv_device*);
struct sv_device** sv_bios_autocycle(struct sv_device*);

#endif
