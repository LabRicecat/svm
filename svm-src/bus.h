#ifndef SVM_BUS_H
#define SVM_BUS_H

#include <stdint.h>

#define SV_BIT(N) uint##N##_t

typedef SV_BIT(8)   sv_byte;
typedef SV_BIT(32)  sv_word;
typedef SV_BIT(32)  sv_rel; // 24
typedef SV_BIT(8)   sv_blk;
typedef SV_BIT(32)  sv_fd; // [8b blk] [24b pos]

typedef sv_fd   (*sv_bwrite)   (sv_fd, sv_byte);
typedef sv_byte (*sv_bread)    (sv_fd);

sv_fd  sv_mkfd (sv_blk, sv_rel);
sv_blk sv_fdblk(sv_fd);
sv_rel sv_fdrel(sv_fd);
sv_fd  sv_fdsuc(sv_fd, sv_byte);
sv_fd  sv_fdseek(sv_fd, sv_rel);

struct sv_bentry {
    sv_blk      block;
    sv_bread    read;
    sv_bwrite   write;
};

struct sv_bentry sv_bentry_register(sv_bread, sv_bwrite);

#define SV_BENTRIES_MAX (sizeof(sv_blk) * CHAR_BIT)
struct sv_bentry* sv_bentries(void);

sv_fd sv_write(sv_fd, sv_byte);
sv_fd sv_write32(sv_fd, sv_word);
sv_byte sv_read(sv_fd);

#endif
