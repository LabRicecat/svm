#include "blockmapper.h"

#include "devices/RAM.h"
#include "devices.h"
#include "devices/CPU.h"
#include "devices/drive.h"
#include "devices/display.h"
#include "devices/ROM.h"
#include "devices/keyboard.h"

void sv_blockmapper(void) {
    struct sv_device* ram = sv_get_ram();
  
    sv_dwrite(ram, SV_FIXEDMAPPED_RANGE, SV_FIXEDMAPPED_SIZE);
    sv_dwrite(ram, SV_FIXEDMAPPED_RAM, sv_dblock(ram));
    sv_dwrite(ram, SV_FIXEDMAPPED_CPU, sv_dblock(sv_get_cpu()));
    sv_dwrite(ram, SV_FIXEDMAPPED_DRIVE, sv_dblock(sv_get_drive()));
    sv_dwrite(ram, SV_FIXEDMAPPED_DISPLAY, sv_dblock(sv_get_display()));

    sv_dwrite(ram, SV_FIXEDMAPPED_ROM, sv_dblock(sv_get_rom()));
    sv_dwrite(ram, SV_FIXEDMAPPED_KEYBOARD, sv_dblock(sv_get_keyboard()));
}
